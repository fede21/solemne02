/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;


import cl.dao.PersonasJpaController;
import cl.entity.Personas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Federico
 */
@WebServlet(name = "Registro_controlador", urlPatterns = {"/Registro_controlador"})
public class Registro_controlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Registro_controlador</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Registro_controlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
      //processRequest(request, response);
       
      
              // recuperar campos del formulario
      
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String sexo = request.getParameter("sexo");
            String edad = request.getParameter("edad");
            
            
            String accion = request.getParameter("accion");
            
            
            Personas persona = new Personas();
            
            // designa variables al objeto persona
            
            persona.setRut(rut);
            persona.setNombre(nombre);
            persona.setApellido(apellido);
            persona.setSexo(sexo);
            persona.setEdad(edad);
            
            PersonasJpaController dao= new PersonasJpaController();
            
           
            
            
             try {
            
                 //envia los datos a la base de datos
                 
            dao.create(persona);  
            request.setAttribute("persona", persona);
           request.getRequestDispatcher("resultado.jsp").forward(request, response);
            System.out.println("entrooo");
            
        } catch (Exception ex) {
            Logger.getLogger(Registro_controlador.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("no entro");
        }
            
    
            
            
           
            
            
            
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}       
