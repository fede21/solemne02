/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.dao.PersonasJpaController;
import cl.dao.exceptions.NonexistentEntityException;
import cl.entity.Personas;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Federico
 */
@WebServlet(name = "Editar_controlador", urlPatterns = {"/Editar_controlador"})
public class Editar_controlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Editar_controlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Editar_controlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String rut = request.getParameter("rut");
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String sexo = request.getParameter("sexo");
        String edad = request.getParameter("edad");

        String accion = request.getParameter("accion");

        Personas persona = new Personas();
        persona.setRut(rut);
        persona.setNombre(nombre);
        persona.setApellido(apellido);
        persona.setSexo(sexo);
        persona.setEdad(edad);

        Personas mensaje = new Personas();  // mensaje que mostrara en la siguiente pantalla

        PersonasJpaController dao = new PersonasJpaController();

        if (accion.equals("editar")) {  // aqui editamos usuaio

            try {
                
                dao.edit(persona);
                request.setAttribute("persona", persona);
                request.getRequestDispatcher("resultado_edicion.jsp").forward(request, response);

            } catch (Exception ex) {
                Logger.getLogger(Editar_controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        } // fin de if editar

        if (accion.equals("eliminar")) {

            try {
                dao.destroy(rut);
                request.getRequestDispatcher("borrado.jsp").forward(request, response);

            } catch (NonexistentEntityException ex) {
                Logger.getLogger(Editar_controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        } // fin if eliminar

        if (accion.equals("agregar")) {

            request.getRequestDispatcher("index.jsp").forward(request, response);

        }  // fin if de agregar

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
