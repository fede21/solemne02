<%-- 
    Document   : demo
    Created on : Jun 10, 2021, 10:39:32 AM
    Author     : Federico
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <h1 align="center">Registro Clientes</h1>
        <form name="form1" action="Registro_controlador" method="POST">
            <table class="table table-hover" align="center">
                <thead>

                </thead>
                <tbody>
                    <tr>
                        <td><label> Rut:</label></td>
                        <td><input type="text" class="form-control" name="rut" id="rut" placeholder="Campo de texto"></td>
                    </tr>
                    <tr>
                        <td><label> Nombre</label></td>
                        <td><input type="text" class="form-control" name="nombre" id="nombre" placeholder="Campo de texto"></td>
                    </tr>
                    <tr>
                        <td><label> Apellido:</label></td>
                        <td><input type="text" class="form-control" name="apellido" id="apellido" placeholder="Campo de texto"></td>
                    </tr>
                    <tr>
                        <td><label> Sexo:</label></td>
                        <td><select id="sexo" name="sexo" onchange="ShowSelected();">
                                <option value="hombre">Hombre</option>
                                <option value="mujer">Mujer</option> 
                            </select></td>
                    </tr>
                    <tr>
                        <td><label> Edad:</label></td>
                        <td><input type="text" class="form-control" name="edad" id="edad" placeholder="Campo de texto"></td>
                    </tr>
                    <tr>

                        <td align="center"> <Button type="submit" value="Agregar" name="accion">Agregar</button></td>



                    </tr>

                </tbody>
            </table>
        </form>

    </body>
</html>
