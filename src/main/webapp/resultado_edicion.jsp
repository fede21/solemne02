<%-- 
    Document   : Exito
    Created on : 8 jun. 2021, 2:53:13
    Author     : Feder
--%>

<%@page import="cl.entity.Personas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Personas persona = (Personas) request.getAttribute("persona");


%>
<html lang="en">
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="container">
            <h2 align="center">Edicion exitosa</h2>
            <br><br>      


            <form name="form1" action="Editar_controlador" method="POST">
                <table class="table table-hover" align="center">
                    <thead>
                        <tr>
                            <th>Rut</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Sexo</th>
                            <th>Edad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><%= persona.getRut()%></td>
                            <td><%= persona.getNombre()%></td>
                            <td><%= persona.getApellido()%></td>
                            <td><%= persona.getSexo()%></td>
                            <td><%= persona.getEdad()%></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><button type="submit" name="accion" value="agregar"> Agregar otro usuario</button></td>

                            <td></td>
                            <td></td>
                        </tr>

                    </tbody>
                </table>
            </form>

        </div>

    </body>
</html>